﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Hierachy
{
    public partial class Page1 : ContentPage
    {
        private double n1, n2, r;
        string o;


        public Page1()
        {
            InitializeComponent();
            Title = "Calculator";
        }

        public void Handle_Appearing(object sender, System.EventArgs e)
        {
            DisplayAlert("Hi", "Welcome to the calculator", "Start");

        }

        public void Calculator(object sender, EventArgs e)
        {
            // Check for errors in nb1
            bool isNumeric = double.TryParse(nb1.Text, out _);

            if (isNumeric)
            {
                n1 = double.Parse(nb1.Text);
            }
            else
            {
                nb1.Text = "You must enter a number";
                result.Text = "Error !";
                return;
            }

            // Check for errors in nb2

            isNumeric = double.TryParse(nb2.Text, out _);

            if (isNumeric)
            {
                n2 = double.Parse(nb2.Text);

            }
            else
            {
                nb2.Text = "You must enter a number";
                result.Text = "Error !";
                return;
            }

            // Check for errors in the operator

            if (opt.Text.Contains("+") || opt.Text.Contains("-") ||
                opt.Text.Contains("/") || opt.Text.Contains("*"))
            {
                if (opt.Text.Length != 1)
                {
                    opt.Text = "You must choose only one operator";
                    result.Text = "Error !";
                    return;
                }
                else
                    o = opt.Text;
            }
            else
            {
                opt.Text = "The only operators available are +,-,/,*";
                result.Text = "Error !";
                return;
            }

            switch (o)
                {
                    case "+":
                        r = n1 + n2;
                        break;
                    case "x":
                        r = n1 * n2;
                        break;
                    case "-":
                        r = n1 - n2;
                        break;
                    case "/":
                        r = n1 / n2;
                        break;
                    default:
                        break;
                }

                result.Text = "The result is " + r.ToString();
            n1 = 0;
            n2 = 0;
            o = "";

            }
    }
}
