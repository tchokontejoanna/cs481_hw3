﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace Hierachy
{
    public partial class Page2_Details : ContentPage
    {
        public Page2_Details()
        {
            InitializeComponent();
            Title = "Details about Papio Anubis";
        }

        async void GoBackToMainPage(object sender, EventArgs e)
        {
            bool answer = await DisplayAlert("Question?", "Would you like to go back to the main page ?", "Yes", "No");
            if (answer)
            {
                await Navigation.PopToRootAsync();

            }
        }

        async void Alert()
        {
            await DisplayAlert("Goodbye", "Now you know more on papio anubis", ":)");
        }

        public void Handle_Disappearing(object sender, EventArgs e)
        {
            Alert();
        }
    }
}
