﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Hierachy
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            Title = "Hierachy games";
        }

        async void Page1(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page1());
        }

        async void Page2(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page2());
        }

        async void Page3(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page3());
        }
    }
}
