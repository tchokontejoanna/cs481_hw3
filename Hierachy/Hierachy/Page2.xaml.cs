﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Xamarin.Forms;

namespace Hierachy
{

    /*
     * Tutorial for timer
     * Link https://www.youtube.com/watch?v=BOx1_fsC-VM
    */

    public partial class Page2 : ContentPage
    {
        Stopwatch stopwatch;
        bool c;

        public Page2()
        {
            InitializeComponent();
            var image = new Image { Source = "papio.jpg" };
            image.Source = Device.RuntimePlatform == Device.Android
                            ? ImageSource.FromFile("papio.jpg")
                            : ImageSource.FromFile("Images/papio.jpg");

            stopwatch = new Stopwatch();
            Title = "Guess the name";
            time.Text = "00:00:00:00000";
            c = false;
            
        }

        async void Details(object sender, EventArgs e)
        {
            stopwatch.Reset();
            time.Text = "00:00:00:00000";
            if (c)
            {
                c = false;
                await Navigation.PushAsync(new Page2_Details());
            }
            else
            {
                await DisplayAlert(":/", "Sorry !", "You have to find the answer first");
                Handle_Appearing(sender, e);
            }

        }

        public void Handle_Appearing(object sender, System.EventArgs e)
        {
            stopwatch.Start();

            Device.StartTimer(TimeSpan.FromMilliseconds(100), () =>
            {
                time.Text = stopwatch.Elapsed.ToString();
                return true;
            }
            );
        }

        public void Response(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (button.Text == "Papio Anubis")
            {
                stopwatch.Stop();
                c = true;
                DisplayAlert("Congratulations :)", "Click details to no more about the Papio Anubis", "Got it");

            }
            else
            {
                DisplayAlert(":(", "Wrong answer", "Try again");
            }
        }

    }
}
